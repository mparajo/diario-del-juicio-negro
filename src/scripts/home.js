var menu = function() { 
    var mainnav = document.getElementById("main-nav");
    var handler = document.getElementById("main-nav-handler"); 
    mainnav.classList.toggle("mostrar");
    handler.classList.toggle("cerrar");
}
document.getElementById("main-nav-handler").addEventListener("click",menu,false);

var verMas = function() {
    var textBlock = document.getElementById("js-text-content");
    var textBlockHandler = document.getElementById("js-abrir-text-content");
    textBlock.classList.toggle("open");
    textBlockHandler.classList.toggle("cerrar");
    var textHandler = textBlockHandler.textContent;
    if(textHandler == 'Ver más') {
        textBlockHandler.textContent = "Ver menos";
    } else {
        textBlockHandler.textContent = "Ver más";
    }
}
document.getElementById("js-abrir-text-content").addEventListener("click",verMas,false);

/****************************************** */

function loadDoc(xy) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var xmlDoc = this.responseXML;
            var x = xmlDoc.getElementsByTagName("video");
            var numero = xy;
            var insertarText = '<h3>' + 
                        x[numero].getElementsByTagName("title")[0].childNodes[0].nodeValue + 
                        '</h3>' + 
                        x[numero].getElementsByTagName("text")[0].childNodes[0].nodeValue;
            var insertarVideo = '<iframe id="vrudo" class="video-embed__vrudo" src="' + x[numero].getElementsByTagName("src")[0].childNodes[0].nodeValue + ' " width="100%" height="100%" allowscriptaccess="always" allowfullscreen="true" webkitallowfullscreen="true" frameborder="0" scrolling="no" allow="autoplay; fullscreen" loading="lazy"></iframe>';
            document.getElementById("js-video-text-content").innerHTML = insertarText;
            document.getElementById("js-videoentrevista").innerHTML = insertarVideo;

        }
    };
    xhttp.open("GET", "../assets/data/videos.xml", true);
    xhttp.send();
}

function cargar() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            listado(this);
        }
    };
    xhttp.open("GET", "../assets/data/videos.xml", true);
    xhttp.send();
}

function listado(xml) {
    var i;
    var xmlDoc = xml.responseXML;
    var title = xmlDoc.getElementsByTagName("title");
    var fotito = xmlDoc.getElementsByTagName("foto");
    var source = xmlDoc.getElementsByTagName("src");
    var columna = "";
    for (i = 0; i < title.length; i++) {
        columna += '<li><button onclick="loadDoc(' + i + ')"></button><img src="./assets/fotos/videos/previews/' + fotito[i].childNodes[0].nodeValue + '" alt="' + title[i].childNodes[0].nodeValue + '"><span>' + title[i].childNodes[0].nodeValue + '</span></li>'; 
    }
    document.getElementById("js-video-lista").innerHTML = columna;
}

Window.onload = cargar();